import React, { useState } from 'react';
import {
    initializeBlock,
    Box,
    Text,
    Button,
    Switch,
    Label,
    Input,
    useViewport
} from '@airtable/blocks/ui';
import styled from 'styled-components';
import { I18nextProvider, useTranslation } from 'react-i18next';
import i18n from './i18n';

import GeneratorContextProvider from './context/GeneratorContext';
import { 
    ClearData, 
    FieldMapper, 
    Records, 
    GeneratorButton, 
    TableSelector
} from './components'; 

const PaddedBox = styled(Box)`
  margin-bottom: 10px
`;

const AppBlock = () => {
    const [ table, setTable ] = useState(null);
    const [ merge, setMerge ] = useState(true);
    const [ recordCount, setRecordCount ] = useState(5);
    const { t } = useTranslation();
    const viewport = useViewport();

    const toggleFullscreen = () => {
        if (viewport.isFullscreen) {
            viewport.exitFullscreen();
        } else {
            viewport.enterFullscreenIfPossible();
        }
    }

    return (
        <GeneratorContextProvider>
            {
                viewport.isFullscreen ? (
                    <>
                        <Box
                            display="inline-block"
                            alignItems="center"
                            justifyContent="center"
                            border="default"
                            backgroundColor="white"
                            padding={5}
                            width={"40%"}
                            height={"90vh"}
                            overflow="hidden"
                            overflowY="scroll"
                        >
                            <TableSelector table={table} setTable={setTable} />
                            <PaddedBox>
                                <Label>{t('number_of_records')}</Label>
                                <Input value={recordCount} type="number" onChange={(e) => setRecordCount(e.target.value)} />
                            </PaddedBox>
                            <PaddedBox>
                                <Label>{t('merge_existing_data')}</Label>
                                <Switch
                                    value={merge}
                                    onChange={newValue => setMerge(newValue)}
                                    label={t('merge_existing_data')}
                                />
                            </PaddedBox>
                            <Box>
                                <Label>{t('field_mapping')}</Label>
                                <PaddedBox><small>{t('field_mapping_small_print')}</small></PaddedBox>
                                {
                                    !table ? <Text textAlign="center" textColor="light">{t('no_field_mapping')}</Text>: (
                                        table.fields.map((item) => (   
                                            <FieldMapper key={item.id} fieldId={item.id} name={item.name} type={item.type} />
                                        ))
                                    )
                                }
                            </Box>
                        </Box>
                        <Box
                            display="inline-block"
                            alignItems="center"
                            justifyContent="center"
                            border="default"
                            width={"60%"}
                            height={"90vh"}
                            backgroundColor="white"
                            padding={5}
                            overflow="hidden"
                            overflowY="scroll"
                        >
                            <Box>
                                <Label>{t('generated_data')}</Label>
                                <Records table={table} />
                            </Box>
                        </Box>
                        <Box
                        display="flex"
                        alignItems="flex-end"
                        justifyContent="flex-end"
                        backgroundColor="white"
                        overflow="hidden"
                    >
                        <ClearData table={table} />
                        <GeneratorButton table={table} recordCount={recordCount} />
                    </Box>
                    </>
                ): (
                    <Box
                        display="flex"
                        alignItems="center"
                        justifyContent="center"
                        border="default"
                        backgroundColor="white"
                        padding={5}
                        width={"100%"}
                        height={"100vh"}
                        overflow="hidden"
                    >
                        <Button
                            onClick={() => toggleFullscreen()}
                            variant="primary"
                            icon="fullscreen"
                        >{t('get_started_text')}</Button>
                    </Box>
                    
                )
            }
            
        </GeneratorContextProvider>
        
    )
}

initializeBlock(() => <I18nextProvider i18n={i18n}><AppBlock /></I18nextProvider>);
