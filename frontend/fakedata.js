// Functions responsible for matching select options to fake data to generate

const DataObject = (faker) => {
    return (
        {
            //Start Internet
            email: { label: "Email", value: faker ? faker.internet.email : 'Email' },
            avatar: { label: "Avatar", value: faker ? faker.internet.avatar : 'Avatar' },
            userName: { label: "UserName", value: faker ? faker.internet.userName : 'UserName' },
            protocol: { label: "Protocol", value: faker ? faker.internet.protocol : 'Protocol'},
            domainSuffix: { label: "DomainSuffix", value: faker ? faker.internet.domainSuffix : 'DomainSuffix' },
            domainWord: { label: "DomainWord", value: faker ? faker.internet.domainWord : 'DomainWord'},
            exampleEmail: { label: "Example Email", value: faker ? faker.internet.exampleEmail : 'Example Email'},
            domainName: { label: "Domain Name", value: faker ? faker.internet.domainName : 'Domain Name'},
            ip: { label: "IP Address", value: faker ? faker.internet.ip : 'IP Address'},
            ipv6: { label: "IP ADDRESS Version 6", value: faker ? faker.internet.ipv6 : 'IP Address Version 6'},
            userAgent: { label: "User Agent", value: faker ? faker.internet.userAgent : 'User Agent'},
            color: { label: "Color", value: faker ? faker.internet.color : 'Color'},
            mac: { label: "Mac address", value: faker ? faker.internet.mac : 'Mac address'},
            password: { label: "Password", value: faker ? faker.internet.password : 'Password'},
            //End Internet
            //Start Addresses
            zipCode: { label: "Zip Code", value: faker ? faker.address.zipCode : 'Zip Code' },
            city: { label: "City", value: faker ? faker.address.city : 'City' },
            cityPrefix: { label: "City Prefix", value: faker ? faker.address.cityPrefix : 'City Prefix' },
            citySuffix: { label: "City Suffix", value: faker ? faker.address.citySuffix : 'City Suffix' },
            streetName: { label: "Street Name", value: faker ? faker.address.streetName : 'Street Name' },
            streetAddress: { label: "Street Address", value: faker ? faker.address.streetAddress : 'Street Address' },
            streetSuffix: { label: "Street Suffix", value: faker ? faker.address.streetSuffix : 'Street Suffix' },
            streetPrefix: { label: "Street Prefix", value: faker ? faker.address.streetPrefix : 'Street Prefix' },
            secondaryAddress: { label: "Secondary Address", value: faker ? faker.address.secondaryAddress : 'Secondary Address' },
            county: { label: "County", value: faker ? faker.address.county : 'County' },
            country: { label: "Country", value: faker ? faker.address.country : 'Country' },
            countryCode: { label: "Country Code", value: faker ? faker.address.countryCode : 'Country Code' },
            state: { label: "State", value: faker ? faker.address.state : 'State' },
            stateAbbr: { label: "State Abbreviation", value: faker ? faker.address.stateAbbr : 'State Abbreviation' },
            latitude: { label: "Latitude", value: faker ? faker.address.latitude : 'Latitude' },
            longitude: { label: "Longitude", value: faker ? faker.address.longitude : 'Longitude' },
            //End Address
            //Start Commerce
            Commercecolor: { label: "Commerce Color", value: faker ? faker.commerce.color : 'Commerce Color' },
            department: { label: "Department", value: faker ? faker.commerce.department : 'Department' },
            productName: { label: "Product Name", value: faker ? faker.commerce.productName : 'Product Name' },
            price: { label: "Price", value: faker ? faker.commerce.price : 'Price' },
            productAdjective: { label: "Product Adjective", value: faker ? faker.commerce.productAdjective : 'Product Adjective' },
            productMaterial: { label: "Product Material", value: faker ? faker.commerce.productMaterial : 'Product Material' },
            product: { label: "Product", value: faker ? faker.commerce.product : 'Product' },
            productDescription: { label: "Product Description", value: faker ? faker.commerce.productDescription : 'Product Description' },
            //End Commerce
            //Start Company
            suffixes: { label: "Suffixes", value: faker ? faker.company.suffixes : 'Suffixes' },
            companyName: { label: "Company Name", value: faker ? faker.company.companyName : 'Company Name' },
            companySuffix: { label: "Company Suffix", value: faker ? faker.company.companySuffix : 'Company Suffix' },
            catchPhrase: { label: "Catch Phrase", value: faker ? faker.company.catchPhrase : 'Catch Phrase' },
            bs: { label: "Company Bs", value: faker ? faker.company.bs : 'Company Bs' },
            catchPhraseAdjective: { label: "Catch Phrase Adjective", value: faker ? faker.company.catchPhraseAdjective : 'Catch Phrase Adjective' },
            catchPhraseDescriptor: { label: "Catch Phrase Descriptor", value: faker ? faker.company.catchPhraseDescriptor : 'Catch Phrase Descriptor' },
            catchPhraseNoun: { label: "Catch Phrase Nound", value: faker ? faker.company.catchPhraseNoun : 'Catch Phrase Nound' },
            bsAdjective: { label: "Company BS adjective", value: faker ? faker.company.bsAdjective : 'Company BS adjective' },
            bsBuzz: { label: "Bs Buzz", value: faker ? faker.company.bsBuzz : 'Bs Buzz' },
            bsNoun: { label: "Bs Noun", value: faker ? faker.company.bsNoun : 'Bs Noun' },
            //End Company
            //Start Date
            past: { label: "Past Date", value: faker ? faker.date.past : 'Past Date' },
            future: { label: "Future Date", value: faker ? faker.date.future : 'Future Date' },
            between: { label: "Between Date", value: faker ? faker.date.between : 'Between Date' },
            recent: { label: "Recent Date", value: faker ? faker.date.recent : 'Recent Date' },
            soon: { label: "Soon Date", value: faker ? faker.date.soon : 'Soon Date' },
            month: { label: "Month", value: faker ? faker.date.month : 'Month' },
            weekday: { label: "Weekday", value: faker ? faker.date.weekday : 'Weekday' },

            //End Date
            //Start Finance
            account: { label: "Account", value: faker ? faker.finance.account : 'Account' },
            accountName: { label: "Account Name", value: faker ? faker.finance.accountName : 'Account Name' },
            mask: { label: "Mask", value: faker ? faker.finance.mask : 'Mask' },
            amount: { label: "Amount", value: faker ? faker.finance.amount : 'Amount' },
            transactionType: { label: "Transaction Type", value: faker ? faker.finance.transactionType : 'Transaction Type' },
            currencyCode : { label: "Currency Code", value: faker ? faker.finance.currencyCode : 'Currency Cod' },
            currencyName: { label: "Currency Name", value: faker ? faker.finance.currencyName : 'Currency Name' },
            currencySymbol: { label: "Currency Symbol", value: faker ? faker.finance.currencySymbol : 'Currency Symbol' },
            bitcoinAddress: { label: "Bitcoin Address", value: faker ? faker.finance.bitcoinAddress : 'Bitcoin Address' },
            ethereumAddress: { label: "Ethereum Address", value: faker ? faker.finance.ethereumAddress : 'Ethereum Address' },
            iban: { label: "IBAN", value: faker ? faker.finance.iban : 'IBAN' },
            bic: { label: "BIC", value: faker ? faker.finance.bic : 'BIC' },
            //End Finance
            //Start Images
            image: { label: "Images", value: faker ? faker.image.image : 'Images' },
            avatarImages: { label: "Avatar Images", value: faker ? faker.image.avatar : 'Avatar Images' },
            imageUrl: { label: "Images Url", value: faker ? faker.image.imageUrl : 'Images Url' },
            abstract: { label: "Abstract Images", value: faker ? faker.image.abstract : 'Abstract Images' },
            animals: { label: "Animals Images", value: faker ? faker.image.animals : 'Animals Images' },
            business: { label: "Business Images", value: faker ? faker.image.business : 'Business Images' },
            cats: { label: "Cats Images", value: faker ? faker.image.cats : 'Cats Images' },
            cityImages: { label: "City Images", value: faker ? faker.image.city : 'City Images' },
            food: { label: "Food Images", value: faker ? faker.image.food : 'Food Images' },
            nightlife: { label: "Night Life Images", value: faker ? faker.image.nightlife : 'Night Life Images' },
            fashion: { label: "Fashion Images", value: faker ? faker.image.fashion : 'Fashion Images' },
            people: { label: "People Images", value: faker ? faker.image.people : 'People Images' },
            nature: { label: "Nature Images", value: faker ? faker.image.nature : 'Nature Images' },
            sports: { label: "Sports Images", value: faker ? faker.image.sports : 'Sports Images' },
            technics: { label: "Technics Images", value: faker ? faker.image.technics : 'Technics Images' },
            transport: { label: "Transport Images", value: faker ? faker.image.transport : 'Transport Images' },
            //End Images
            //Start Lorem
            word: { label: "Word", value: faker ? faker.lorem.word : 'Word' },
            words: { label: "Words", value: faker ? faker.lorem.words : 'Words' },
            sentence: { label: "Sentence", value: faker ? faker.lorem.sentence : 'Sentence' },
            slug: { label: "Slug", value: faker ? faker.lorem.slug : 'Slug' },
            sentences: { label: "Sentences", value: faker ? faker.lorem.sentences : 'Sentences' },
            paragraph: { label: "Paragraph", value: faker ? faker.lorem.paragraph : 'Paragraph' },
            paragraphs: { label: "Paragraphs", value: faker ? faker.lorem.paragraphs : 'Paragraphs' },
            text: { label: "Text", value: faker ? faker.lorem.text : 'Text' },
            lines: { label: "Lines", value: faker ? faker.lorem.lines : 'Lines' },
            //End Lorem
            // Start Phone
            phoneNumber: { label: "Phone Number", value: faker ? faker.phone.phoneNumber : 'Phone Number' },
            phoneNumberFormat: { label: "Phone Number Format", value: faker ? faker.phone.phoneNumberFormat : 'Phone Number Format' },
            phoneFormats: { label: "Phone Format", value: faker ? faker.phone.phoneFormats : 'Phone Formats' },
            // End Phone
            //Start Random
            number: { label: "Number", value: faker ? faker.random.number : 'Number' },
            float: { label: "Float", value: faker ? faker.random.float : 'Float' },
            uuid: { label: "UUID", value: faker ? faker.random.uuid : 'UUID' },
            boolean: { label: "Boolean", value: faker ? faker.random.boolean : 'Boolean' },
            locale: { label: "Locale", value: faker ? faker.random.locale : 'Locale' },
            alphaNumeric: { label: "Alpha Numeric", value: faker ? faker.random.alphaNumeric : 'Alpha Numeric' },
            hexaDecimal: { label: "Hexa Decimal", value: faker ? faker.random.hexaDecimal : 'Hexa Decimal' },
            //End Random
            //Start Name
            firstName: { label: "First Name", value: faker ? faker.name.firstName : 'First Name' },
            lastName: { label: "Last Name", value: faker ? faker.name.lastName : 'Last Name' },
            jobTitle: { label: "Job Title", value: faker ? faker.name.jobTitle : 'Job Title' },
            title: { label: "Person Title", value: faker ? faker.name.title : 'Person Title' },
            jobDescriptor: { label: "Job Descriptor", value: faker ? faker.name.jobDescriptor : 'Job Descriptor' },
            jobType: { label: "Job Type", value: faker ? faker.name.jobType : 'Job Type' },
            // EndName 

        }
    )
}

const getFieldData = (faker = null) => {
    return Object.values(DataObject(faker))
}

export { getFieldData };

const fakeDataGenerator = (label, faker) => {
    switch(label){
        case DataObject(faker).email.label:
            return DataObject(faker).email.value();
        case DataObject(faker).avatar.label:
            return DataObject(faker).avatar.value();
        case DataObject(faker).userName.label:
            return DataObject(faker).userName.value();
        case DataObject(faker).protocol.label:
            return DataObject(faker).protocol.value();
        case DataObject(faker).domainSuffix.label:
            return DataObject(faker).domainSuffix.value();
        case DataObject(faker).domainWord.label:
            return DataObject(faker).domainWord.value();
        case DataObject(faker).exampleEmail.label:
            return DataObject(faker).exampleEmail.value();
        case DataObject(faker).domainName.label:
            return DataObject(faker).domainName.value();
        case DataObject(faker).ip.label:
            return DataObject(faker).ip.value();
        case DataObject(faker).ipv6.label:
            return DataObject(faker).ipv6.value();
        case DataObject(faker).userAgent.label:
            return DataObject(faker).userAgent.value();
        case DataObject(faker).color.label:
            return DataObject(faker).color.value();
        case DataObject(faker).mac.label:
            return DataObject(faker).mac.value();
        case DataObject(faker).password.label:
            return DataObject(faker).password.value();
        // Address
        case DataObject(faker).zipCode.label:
            return DataObject(faker).zipCode.value();
        case DataObject(faker).city.label:
            return DataObject(faker).city.value();
        case DataObject(faker).cityPrefix.label:
            return DataObject(faker).cityPrefix.value();
        case DataObject(faker).streetName.label:
            return DataObject(faker).streetName.value();
        case DataObject(faker).citySuffix.label:
            return DataObject(faker).citySuffix.value();
        case DataObject(faker).streetAddress.label:
            return DataObject(faker).streetAddress.value();
        case DataObject(faker).streetSuffix.label:
            return DataObject(faker).streetSuffix.value();
        case DataObject(faker).streetPrefix.label:
            return DataObject(faker).streetPrefix.value();
        case DataObject(faker).secondaryAddress.label:
            return DataObject(faker).secondaryAddress.value();
        case DataObject(faker).county.label:
            return DataObject(faker).county.value();
        case DataObject(faker).country.label:
            return DataObject(faker).country.value();
        case DataObject(faker).countryCode.label:
            return DataObject(faker).countryCode.value();
        case DataObject(faker).state.label:
            return DataObject(faker).state.value();
        case DataObject(faker).latitude.label:
            return DataObject(faker).latitude.value();
        case DataObject(faker).longitude.label:
            return DataObject(faker).longitude.value();  
        // Commerce
        case DataObject(faker).Commercecolor.label:
            return DataObject(faker).Commercecolor.value();
        case DataObject(faker).department.label:
            return DataObject(faker).department.value();
        case DataObject(faker).productName.label:
            return DataObject(faker).productName.value();
        case DataObject(faker).price.label:
            return DataObject(faker).price.value();
        case DataObject(faker).productAdjective.label:
            return DataObject(faker).productAdjective.value();
        case DataObject(faker).productMaterial.label:
            return DataObject(faker).productMaterial.value();
        case DataObject(faker).product.label:
            return DataObject(faker).product.value();
        case DataObject(faker).productDescription.label:
            return DataObject(faker).productDescription.value();        
        case DataObject(faker).suffixes.label:
            return DataObject(faker).suffixes.value();
        case DataObject(faker).companyName.label:
            return DataObject(faker).companyName.value();
        case DataObject(faker).companySuffix.label:
            return DataObject(faker).companySuffix.value();
        case DataObject(faker).catchPhrase.label:
            return DataObject(faker).catchPhrase.value();
        case DataObject(faker).bs.label:
            return DataObject(faker).bs.value();
        case DataObject(faker).catchPhraseAdjective.label:
            return DataObject(faker).catchPhraseAdjective.value();
        case DataObject(faker).catchPhraseDescriptor.label:
            return DataObject(faker).catchPhraseDescriptor.value();       
        case DataObject(faker).catchPhraseNoun.label:
            return DataObject(faker).catchPhraseNoun.value();
        case DataObject(faker).bsAdjective.label:
            return DataObject(faker).bsAdjective.value();
        case DataObject(faker).bsBuzz.label:
            return DataObject(faker).bsBuzz.value();
        case DataObject(faker).bsNoun.label:
            return DataObject(faker).bsNoun.value();   
        case DataObject(faker).past.label:
            return DataObject(faker).past.value();
        case DataObject(faker).future.label:
            return DataObject(faker).future.value();
        case DataObject(faker).between.label:
            return DataObject(faker).between.value();
        case DataObject(faker).recent.label:
            return DataObject(faker).recent.value();
        case DataObject(faker).soon.label:
            return DataObject(faker).soon.value();
        case DataObject(faker).month.label:
            return DataObject(faker).month.value();
        case DataObject(faker).weekday.label:
            return DataObject(faker).weekday.value();
        // Finance
        case DataObject(faker).account.label:
            return DataObject(faker).account.value();
        case DataObject(faker).accountName.label:
            return DataObject(faker).accountName.value();
        case DataObject(faker).mask.label:
            return DataObject(faker).mask.value();
        case DataObject(faker).amount.label:
            return DataObject(faker).amount.value();
        case DataObject(faker).transactionType.label:
            return DataObject(faker).transactionType.value();
        case DataObject(faker).currencyCode.label:
            return DataObject(faker).currencyCode.value();
        case DataObject(faker).currencyName.label:
            return DataObject(faker).currencyName.value();
        case DataObject(faker).currencySymbol.label:
            return DataObject(faker).currencySymbol.value();
        case DataObject(faker).bitcoinAddress.label:
            return DataObject(faker).bitcoinAddress.value();
        case DataObject(faker).ethereumAddress.label:
            return DataObject(faker).ethereumAddress.value();
        case DataObject(faker).iban.label:
            return DataObject(faker).iban.value();
        case DataObject(faker).bic.label:
            return DataObject(faker).bic.value();  
        // Images 
        case DataObject(faker).image.label:
            return DataObject(faker).image.value();
        case DataObject(faker).avatarImages.label:
            return DataObject(faker).avatarImages.value();
        case DataObject(faker).abstract.label:
            return DataObject(faker).abstract.value();
        case DataObject(faker).imageUrl.label:
            return DataObject(faker).imageUrl.value();
        case DataObject(faker).animals.label:
            return DataObject(faker).animals.value();
        case DataObject(faker).business.label:
            return DataObject(faker).business.value();
        case DataObject(faker).cats.label:
            return DataObject(faker).cats.value();
        case DataObject(faker).cityImages.label:
            return DataObject(faker).cityImages.value();
        case DataObject(faker).food.label:
            return DataObject(faker).food.value();
        case DataObject(faker).nightlife.label:
            return DataObject(faker).nightlife.value();
        case DataObject(faker).fashion.label:
            return DataObject(faker).fashion.value();
        case DataObject(faker).people.label:
            return DataObject(faker).people.value();
        case DataObject(faker).nature.label:
            return DataObject(faker).nature.value();
        case DataObject(faker).sports.label:
            return DataObject(faker).sports.value();
        case DataObject(faker).technics.label:
            return DataObject(faker).technics.value();
        case DataObject(faker).transport.label:
            return DataObject(faker).transport.value();
        // Lorem 
        case DataObject(faker).word.label:
            return DataObject(faker).word.value();  
        case DataObject(faker).words.label:
            return DataObject(faker).words.value();  
        case DataObject(faker).sentence.label:
            return DataObject(faker).sentence.value();  
        case DataObject(faker).slug.label:
            return DataObject(faker).slug.value();  
        case DataObject(faker).sentences.label:
            return DataObject(faker).sentences.value();  
        case DataObject(faker).paragraph.label:
            return DataObject(faker).paragraph.value();  
        case DataObject(faker).paragraphs.label:
            return DataObject(faker).paragraphs.value();  
        case DataObject(faker).text.label:
            return DataObject(faker).text.value();  
        case DataObject(faker).lines.label:
            return DataObject(faker).lines.value();  
        // Phone
        case DataObject(faker).phoneNumber.label:
            return DataObject(faker).phoneNumber.value(); 
        case DataObject(faker).phoneNumberFormat.label:
            return DataObject(faker).phoneNumberFormat.value(); 
        case DataObject(faker).phoneFormats.label:
            return DataObject(faker).phoneFormats.value(); 
        // Random 
        case DataObject(faker).number.label:
            return DataObject(faker).number.value(); 
        case DataObject(faker).float.label:
            return DataObject(faker).float.value(); 
        case DataObject(faker).uuid.label:
            return DataObject(faker).uuid.value(); 
        case DataObject(faker).boolean.label:
            return DataObject(faker).boolean.value(); 
        case DataObject(faker).locale.label:
            return DataObject(faker).locale.value(); 
        case DataObject(faker).alphaNumeric.label:
            return DataObject(faker).alphaNumeric.value();        
        case DataObject(faker).hexaDecimal.label:
            return DataObject(faker).hexaDecimal.value();       
        // Name
        case DataObject(faker).firstName.label:
            return DataObject(faker).firstName.value();
        case DataObject(faker).lastName.label:
            return DataObject(faker).lastName.value();
        case DataObject(faker).jobTitle.label:
            return DataObject(faker).jobTitle.value();
        case DataObject(faker).title.label:
            return DataObject(faker).title.value();
        case DataObject(faker).jobDescriptor.label:
            return DataObject(faker).jobDescriptor.value();
        case DataObject(faker).jobType.label:
            return DataObject(faker).jobType.value();
        default:
            return '';
    }
}

export default fakeDataGenerator