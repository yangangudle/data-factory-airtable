import i18n from 'i18next'
import LanguageDetector from "i18next-browser-languagedetector"
import { initReactI18next } from 'react-i18next'
import XHR from 'i18next-xhr-backend'
import languageEN from './locate/en/translate.json'
import languageFR from './locate/fr/translate.json'

i18n
.use(XHR)
.use(LanguageDetector)
.use(initReactI18next)
.init({
    resources: {
        en: languageEN,
        jp: languageFR
    },
    /* default language when load the website in browser */
    lng: "en",
    /* When react i18next not finding any language to as default in borwser */
    fallbackLng: "en",
    /* debugger For Development environment */
    debug: false,
    ns: ["translations"],
    defaultNS: "translations",
    keySeparator: ".",
    interpolation: {
        escapeValue: false,
        formatSeparator: ","
    },
    react: {
        wait: true,
        bindI18n: 'languageChanged loaded',
        bindStore: 'added removed',
        nsMode: 'default'
    }
})

export default i18n;

// Line 1…6 Import all dependency and separated language JSON file in i18n.js file.
// Line 9, using the XHR so language file could be loaded with using Lazy technique
// Line 10, Using the Language Detector, so When the site load in the browser it can detect the language
// Line 11, Hand over the i18n configuration to react-i18next npm package
// Line 13...16, add the `json` formated language `resource` to configuration. Resource are using for get the data and data file to translate process. In our application i load two json file as resource for traslation.
// Line 18,(lng) Select the default language as English
// Line 20, (fallbackLng) is the i18next react not finding any language to load, it will load the english
// Line 23 (ns), 24 (defaultNS), Add the Namespace and Default Namespace, Namespaces are a feature in which allows you to separate translations that get loaded into multiple files. we can select the namespace for starting point for translation, where to get the key and value for traslate. In our json file we use `traslate` as namespace to seperate and start point for traslate.
// Line 25, keySeparator is used for extract value by key for nested object.
// Line 26...29, Interpolation is one of the most used functionalities in I18N. It enables you to integrate dynamic values into your translations.
// react 30...35, Handle rendering while translations are not yet loaded, We can globally configure the wait option by add `react: {wait:true}` 