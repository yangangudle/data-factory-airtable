import React, { createContext, useState } from 'react';
import { globalConfig } from '@airtable/blocks';

export const GeneratorContext = createContext({});

const GeneratorContextProvider = (props) => {
    const [ schema, setSchema] = useState([]);
    const appendSchema = (params) => {
        const newSchema = schema.filter(obj => obj.fieldId != params.fieldId);
        setSchema([...newSchema, params]);
    };
    
    const emptySchema = () => {
        setSchema([]);
    }
  
    const appendRecord = (records, table ) => {
        let existingDataGenerated = globalConfig.get('data_generated');
        if(!existingDataGenerated) {
            existingDataGenerated = []
        }

        const addRecords = records.map((record) => {
            return { record, table: table.id }
        });

        if (globalConfig.hasPermissionToSet('data_generated', [...existingDataGenerated, ...addRecords ])) {
            globalConfig.setAsync('data_generated', [...existingDataGenerated,  ...addRecords ]);
        }
    }

    const addRecord = (records, table) => {
        appendRecord(records, table);
    }

    const removeSchemaRecordById = (id) => {
        const newSchema = schema.filter(obj => obj.fieldId != id);
        setSchema(newSchema);
    } 

    return (
        <GeneratorContext.Provider
            value={{
                schema, 
                emptySchema,
                removeSchemaRecordById,
                appendSchema,
                addRecord
            }}
        >
            {props.children}
        </GeneratorContext.Provider>
    );
};

export default GeneratorContextProvider;
