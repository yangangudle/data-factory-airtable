import React from 'react';
import { useGlobalConfig } from '@airtable/blocks/ui';
import RecordItem from './RecordItem';

const Records = ({ table } ) => { 
    const globalConfig = useGlobalConfig();
    let existingDataGenerated = globalConfig.get('data_generated');
    
    if(!existingDataGenerated) {
        existingDataGenerated = []
    }

    return (
        <>
            {
                existingDataGenerated.map((item) => (
                    <RecordItem key={item.record.id} table={table} recordId={item.record} />
                ))
            }
        </>
    )
}

export default Records;
