import React from 'react';
import {
 Dialog, 
 Loader,
 Heading
} from '@airtable/blocks/ui';

const ProgressBar = ({ isLoading }) => {
    return isLoading && (
        <Dialog width="320px" onClose={() => true} >
            <Heading textAlign="center"><Loader scale={0.4} /></Heading>
        </Dialog>
    )
}

export default ProgressBar;