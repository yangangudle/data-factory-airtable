import React, { useState, useContext } from 'react';
import { Box as AirtableBox, Switch as AirtableSwitch , Icon as AirtableIcon } from '@airtable/blocks/ui';
import styled from 'styled-components';
import SelectBox from './SelectBox';
import { GeneratorContext } from '../context/GeneratorContext';

const Switch = styled(AirtableSwitch)`
    height: 37px;
`;

const Icon = styled(AirtableIcon)`
    margin-top: 6px;
    margin-right: 2vh;
    margin-left: 2vh;
    color: darkgrey
`

const Box = styled(AirtableBox)`
    margin-top: 10px;
`

const FieldMapper = ({ fieldId, name, type }) => {
    const [ show, setShow ] = useState(true);
    const generator = useContext(GeneratorContext);
    
    const switchHandler = (newValue) => {
        setShow(newValue); 
        generator.removeSchemaRecordById(fieldId)
    }

    return (
        <Box width={"100%"}>
            <Box display="flex">
                <Switch
                    width={"35%"}
                    value={show}
                    onChange={newValue => switchHandler(newValue)}
                    label={`${name} (${type})`}
                /> 
                {
                    show && (
                        <>
                            <Icon name="chevronRight" size={20} />
                            <SelectBox fieldId={fieldId} />
                        </>
                    )
                }
                
            </Box>
        </Box>
    )
}

export default FieldMapper;