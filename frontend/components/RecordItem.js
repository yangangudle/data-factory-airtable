import React from 'react';
import { useRecordById, RecordCard as AirtableRecordCard } from '@airtable/blocks/ui';
import styled from 'styled-components';

const RecordCard = styled(AirtableRecordCard)`
    margin-top: 10px;
    margin-bottom: 10px;
`;

const RecordItem = ({ table, recordId }) => {
    const record = useRecordById(table, recordId);
    if(!record) {
        return null;
    }

    return <RecordCard key={recordId} record={record} /> 
}

export default RecordItem;