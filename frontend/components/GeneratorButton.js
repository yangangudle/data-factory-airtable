import React, { useState, useContext, useEffect } from 'react';
import styled from 'styled-components';
import { useTranslation } from 'react-i18next';
import {
    loadScriptFromURLAsync,
    Button as AirtableButton
} from '@airtable/blocks/ui';
import { FieldType } from '@airtable/blocks/models';

import ProgressBar from './ProgressBar';
import { GeneratorContext } from '../context/GeneratorContext';
import fakeDataGenerator from '../fakedata';
const Button = styled(AirtableButton)`
    margin-right: 2%;
    margin-top: 1%;
    margin-bottom: 1%
`;

const GeneratorButton = ({ table, recordCount }) => {
    const generator = useContext(GeneratorContext);
    const [ dataFaker, setDataFaker ] = useState(null);
    const [ isLoading, setIsLoading ] = useState(false);
    const { t } = useTranslation();
    
    useEffect(() => {
        loadScriptFromURLAsync('https://cdnjs.cloudflare.com/ajax/libs/Faker/3.1.0/faker.min.js').then(() => {
            setDataFaker(faker);
        });
    }, []);
    
    const unify = (generator) => {
        const obj = {}
        generator.schema.forEach(item => {
            const field = table.getFieldById(item.fieldId);
            if(field.type === FieldType.MULTIPLE_ATTACHMENTS) {
                Object.assign(obj, {
                    [item.fieldId]: [{ url: fakeDataGenerator(item.dataType, dataFaker)}] 
                })
            } else {
                Object.assign(obj, {
                    [item.fieldId]: fakeDataGenerator(item.dataType, dataFaker)
                })
            }
           
        });
        
        return obj;
    }
   
    const bulkGenerateFakeData = async () => {
        let bulkItems = [];
        setIsLoading(true);
        for (let index = 0; index < recordCount; index++) {
            bulkItems.push({
                fields: unify(generator)
            });
        }
        
        if(table.hasPermissionToCreateRecords(bulkItems)) {
            let i, j, chunk = 50; 
            for(i = 0; j = bulkItems.length, i < j; i+=chunk) {
                const chunked = bulkItems.slice(i, i + chunk);
                const records = await table.createRecordsAsync(chunked);
                generator.addRecord([...records], table)
            }   
        }
        setIsLoading(false);
    }

    return (
        <>
            <ProgressBar isLoading={isLoading} />
            <Button
                disabled={isLoading}
                variant="primary"
                onClick={() => bulkGenerateFakeData()}
                size="large"   
            >{t('generate_data')}</Button>
        </>
    )
}

export default GeneratorButton;