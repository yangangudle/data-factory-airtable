import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import {
    Box as AirtableBox, 
    Label,
    TablePicker
} from '@airtable/blocks/ui';
import styled from 'styled-components';

import { GeneratorContext } from '../context/GeneratorContext';

const Box = styled(AirtableBox)`
    margin-bottom: 10px
`;

const TableSelector = ({ table, setTable }) => {
    const generator = useContext(GeneratorContext);
    const { t } = useTranslation();
    
    const pickTable = (newTable) => {
        setTable(newTable); 
        generator.emptySchema();
    }

    return (
        <Box>
            <Label>{t("choose_a_table")}</Label>
            <TablePicker 
                table={table}
                onChange={newTable => pickTable(newTable)}
            />
        </Box>
    )
}

export default TableSelector;