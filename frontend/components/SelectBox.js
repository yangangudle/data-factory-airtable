import React, { useContext } from 'react'
import Select from 'react-select'
import * as fakedata  from '../fakedata';
import { GeneratorContext } from '../context/GeneratorContext';

const customStyles = {
    control: (provided) => ({
     ...provided,
      width: 200,
      backgroundColor: "#eee",
      color: "darkgrey",
      border: "none"
    }),
    input: (provided) => ({
        ...provided,
        height: 10
    }),
    container: (provided) => ({
        ...provided,
        
    })
}


const SelectBox = ({ fieldId }) => {
    const generator = useContext(GeneratorContext);
    const FieldData = fakedata.getFieldData()
    return <Select options={FieldData} styles={customStyles} onChange={(e) => generator.appendSchema({ fieldId, dataType: e.value })} />
}
  

export default SelectBox;