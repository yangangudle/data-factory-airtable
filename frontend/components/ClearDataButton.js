import React, { useState } from 'react';
import { Button as AirtableButton, useGlobalConfig } from '@airtable/blocks/ui';
import { base } from '@airtable/blocks';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import ProgressBar from './ProgressBar';

const Button = styled(AirtableButton)`
    margin-right: 2%;
    margin-top: 2%;
    margin-bottom: 1%
`;

const ClearData = () => {
    const globalConfig = useGlobalConfig();
    const { t } = useTranslation();
    const [ isLoading, setIsLoading ] = useState(false)
    const existingDataGenerated = globalConfig.get('data_generated');
    
    if(!existingDataGenerated) {
        return null;
    }
    
    /**
     * Check if user has permissions to delete records
     * Then chunk them into groups of 50 and ayncrongously delete from table
    */
    const deleteBulkRecords = (table, toDelete) => {
        if(!table) {
            return false
        }
        
        if (table.hasPermissionToDeleteRecords(toDelete)) { 
            let i, j ,chunk = 50;
            for(i = 0; j = toDelete.length, i < j; i+=chunk) {
                const chunked = toDelete.slice(i, i + chunk);
                table.deleteRecordsAsync(chunked);
            }
        }
    }
    
    /**
     * On click Clear Button 
     * - Start loading screen
     * - Group record data by tables
     * - Bulk delete records
     * - Empty globalConfig storage of data
     * - Remove loading screen
    */
    const deleteRecords = (records) => {
        if(!records) {
            return false;
        }

        setIsLoading(true);
        
        const groupByTable = records.reduce((r, a) => {
            r[a.table] = r[a.table] || [];
            r[a.table].push(a.record);
            return r;
        }, Object.create(null))

        for(let tableKey in groupByTable) {
            // Check if table exists first
            const table = base.getTableByIdIfExists(tableKey);
            const recordsToDelete = groupByTable[tableKey];
            deleteBulkRecords(table, recordsToDelete)
        }
        
        globalConfig.setAsync('data_generated', undefined);
        setIsLoading(false);
    }

    return (
        <>
            <ProgressBar isLoading={isLoading} />
            <Button
                variant="default"
                size="large"
                onClick={() => deleteRecords(existingDataGenerated)}
            >
                {t('clear_data_button')}
            </Button>
        </>
    )
}

export default ClearData;